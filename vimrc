" vimrc

call plug#begin()
Plug 'tpope/vim-sensible'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'altercation/vim-colors-solarized'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-markdown'
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdcommenter'
Plug 'bling/vim-airline'
Plug 'vim-pandoc/vim-pandoc'
Plug 'rust-lang/rust.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes' 
call plug#end()

filetype plugin indent on

" highlighting and color scheme
set background=dark
colorscheme solarized

syntax on

" set options
set number 		" show line numbers
set ruler  		" shows line/column number of cursor
set hlsearch		" highlight search matches
set incsearch		" incremental searching
set ignorecase		" ignore case in searches
set smartcase		" except if upper case letters are typed
set matchtime=5		" tenths of a second to show a matching paren highlight
set showcmd		" show command while typing
set more		" listings pause when the screen is filled
set showmode		" show mode on bottom line
set scrolloff=5		" lines to keep above and below the cursor
set sidescrolloff=5	" chars to keep left and right of the cursor
set history=200		" command buffer history
set noerrorbells	" no beeps
"set wildmode=full	" default
set wildmenu		" during wildcard completion, show choices
set laststatus=2	" all windows have a status line
set viminfo=		" no viminfo files
set formatoptions=cql  " t - autowrap to textwidth
			" c - autowrap comments to textwidth
			" q - allow formatting of comments with :gq
			" l - don't format already long lines
set textwidth=78 	" sets textwidth to 78
set mouse=a

" tab options
set tabstop=4 		" tab is 8 spaces
set expandtab 		" always uses spaces instead of tabs
set softtabstop=4 	" insert 4 spaces when tab is pressed
set shiftwidth=4 	" indent is 4 spaces
set smarttab 		" indent instead of tab at start of line
set shiftround 		" round spaces to nearest shiftwidth multiple
set nojoinspaces 	" don't convert spaces to tabs

" backup options
set nobackup

" statusline stuff
set statusline=[%t]\  	" tail of the filename
set statusline+=[%{strlen(&fenc)?&fenc:'none'}, "file encoding
set statusline+=%{&ff}] "file format
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag
set statusline+=%y      "filetype
set statusline+=%=      "left/right separator
set statusline+=%c,     "cursor column
set statusline+=%l/%L   "cursor line/total lines
set statusline+=\ %P    "percent through file

" stuff for gvim
set guioptions-=T 	" remove toolbar
set guioptions-=t 	" remove tear off menus (annoying)
"set guifont=Terminus\ 9
if has('macunix')
	set guifont=Inconsolata:h14
endif
if has('gui_win32')
    set guifont=Consolas:h14:cANSI
endif


" easy movement keys
"map <c-j> <c-w>j
"map <c-k> <c-w>k
"map <c-l> <c-w>l
"map <c-h> <c-w>h

